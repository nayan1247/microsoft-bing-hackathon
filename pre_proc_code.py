# -*- coding: utf-8 -*-
"""
Created on Sat Jan 16 10:45:06 2016

@author: flusr.in0073
"""

import os
from collections import defaultdict

#Set working directory
os.chdir('F:\996. Hackathons\MicrosoftBing')
#pwd


#Read Training Data
def import_file(filepath):
    rows=[]
    rows=open(filepath, 'r').read().splitlines()
    return rows


#Convert a row into a list using tab sep as option
def create_df(data_list):
    out_list=[]
    for k in range(0,len(data_list)):
        tmp_list=[]
        for j in range(0,len(data_list[k].split("\t"))):
            val=data_list[k].split("\t")[j]
            if val != '':
                tmp_list.append(val)
        out_list.append(tmp_list)
    return out_list
        

#Function to choose the best features from the lot based on how they occur in the 3 classes
#A=[[3,4,6],[5,7,9],[8,23,7]]



#Create a single index to determine the importance of the token
def text_feature_extract(input_list):
    filtered_feature_list=[]
    for big_elem in input_list:
        elem=big_elem[1]
        filtered_feature_list.append([big_elem[0],round(max(float(elem[0])/sum(elem),float(elem[1])/sum(elem),float(elem[2])/sum(elem)),2)]) 
    return filtered_feature_list

#B=text_feature_extract(created_weighed_list(A))


def truncate_text(input_list,feature_list):
    out_elem=[]
    for elem in input_list:
        intersection_list=filter(lambda k:k in feature_list, elem[5].split(' '))
        new_text=''        
        for word in elem[5].split(' '):
            if word in intersection_list:
                new_text=new_text + word + ' '
        
        elem[5]=new_text
        out_elem.append(elem)
    return out_elem

#Main Code
rows_train_data=import_file("F:\\996. Hackathons\\MicrosoftBing\\Data\\BingHackathonTrainingData.txt")
train_data=create_df(rows_train_data)

#Create a list of words which have different distribution across classes
new_data=''
for j in range(0,len(train_data)):
#    if train_data[j][1] == '0':
    new_data=new_data+train_data[j][5]
mystring = new_data.replace(".", " ")    
mystring = mystring.replace("  ", " ")    
def local_freq_items(sentence):
    items = defaultdict(int)
    freq_items = []
    visited = set() 
    for entry in sentence.split(" "):
    #    print entry  
     #   print ('ok')
        if entry not in visited:
            visited.add(entry)
        items[entry] += 1  
        #print items[entry]          
    return items
items=local_freq_items(mystring)
def local_freq_items_list(sentence,items):
    visited = set() 
    second_list=list()
    for entry in sentence.split(" "):
        #print items[entry]
        if entry not in visited:
            visited.add(entry)
            first_list = list()
            first_list.append(entry)
            first_list.append(items[entry])
            second_list.append(first_list)
    return second_list    
new_list=local_freq_items_list(mystring,items)
len(new_list)
#Get topics
new_data=list()
for j in range(0,len(train_data)):
    new_data.append(train_data[j][1])
set(new_data)
coln=list()
for i in range(0,len(new_list)):
    coln.append(new_list[i][0])
    
import pandas as pd
rnames=coln
cnames=(0,1,2)
final_mat = pd.DataFrame(index=rnames, columns=cnames)
final_mat = final_mat.fillna(0) # with 0s rather than NaNs
##############################
#For 1
new_data=''
for j in range(0,len(train_data)):
    if train_data[j][1] == '0':
        new_data=new_data+train_data[j][5]
mystring = new_data.replace(".", " ")    
mystring = mystring.replace("  ", " ")    
def local_freq_items(sentence):
    items = defaultdict(int)
    freq_items = []
    visited = set() 
    for entry in sentence.split(" "):
    #    print entry  
     #   print ('ok')
        if entry not in visited:
            visited.add(entry)
        items[entry] += 1  
        #print items[entry]          
    return items
items=local_freq_items(mystring)
def local_freq_items_list(sentence,items):
    visited = set() 
    second_list=list()
    for entry in sentence.split(" "):
        #print items[entry]
        if entry not in visited:
            visited.add(entry)
            first_list = list()
            first_list.append(entry)
            first_list.append(items[entry])
            second_list.append(first_list)
    return second_list    
new_list=local_freq_items_list(mystring,items)
len(new_list)
for word in new_list:
    #print str(word[0])
    final_mat.loc[word[0],0] = word[1]
##############################
#For 2
new_data=''
for j in range(0,len(train_data)):
    if train_data[j][1] == '1':
        new_data=new_data+train_data[j][5]
mystring = new_data.replace(".", " ")    
mystring = mystring.replace("  ", " ")    
def local_freq_items(sentence):
    items = defaultdict(int)
    freq_items = []
    visited = set() 
    for entry in sentence.split(" "):
    #    print entry  
     #   print ('ok')
        if entry not in visited:
            visited.add(entry)
        items[entry] += 1  
        #print items[entry]          
    return items
items=local_freq_items(mystring)
def local_freq_items_list(sentence,items):
    visited = set() 
    second_list=list()
    for entry in sentence.split(" "):
        #print items[entry]
        if entry not in visited:
            visited.add(entry)
            first_list = list()
            first_list.append(entry)
            first_list.append(items[entry])
            second_list.append(first_list)
    return second_list    
new_list=local_freq_items_list(mystring,items)
len(new_list)
for word in new_list:
    #print str(word[0])
    final_mat.loc[word[0],1] = word[1]
##############################
#For 3
new_data=''
for j in range(0,len(train_data)):
    if train_data[j][1] == '2':
        new_data=new_data+train_data[j][5]
mystring = new_data.replace(".", " ")    
mystring = mystring.replace("  ", " ")    
def local_freq_items(sentence):
    items = defaultdict(int)
    freq_items = []
    visited = set() 
    for entry in sentence.split(" "):
    #    print entry  
     #   print ('ok')
        if entry not in visited:
            visited.add(entry)
        items[entry] += 1  
        #print items[entry]          
    return items
items=local_freq_items(mystring)
def local_freq_items_list(sentence,items):
    visited = set() 
    second_list=list()
    for entry in sentence.split(" "):
        #print items[entry]
        if entry not in visited:
            visited.add(entry)
            first_list = list()
            first_list.append(entry)
            first_list.append(items[entry])
            second_list.append(first_list)
    return second_list    
new_list=local_freq_items_list(mystring,items)
len(new_list)
for word in new_list:
    #print str(word[0])
    final_mat.loc[word[0],2] = word[1]

blank_list=[]
for i in final_mat.index:
    a=final_mat.loc[i,final_mat.columns[0]]
    b=final_mat.loc[i,final_mat.columns[1]]
    c=final_mat.loc[i,final_mat.columns[2]]
    blank_list.append([i,[a,b,c]])
    

features=text_feature_extract(blank_list)

max_threshold=0.5
token=[]
for row in features:
    if row[1]>max_threshold:
        token.append(row[0])
        
train_data_features=truncate_text(train_data,token)



