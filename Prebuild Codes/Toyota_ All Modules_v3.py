# -*- coding: utf-8 -*-
"""
Created on Mon Aug 31 16:57:55 2015

@author: Nayan Dharamshi
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Aug 27 16:44:05 2015

@author: Nayan Dharamshi
"""
import urllib
import re
from BeautifulSoup import BeautifulSoup
import difflib
import string
from nltk.tokenize import word_tokenize
from nltk.tokenize import RegexpTokenizer
from nltk.stem import WordNetLemmatizer
import sklearn.feature_extraction
from sklearn.feature_extraction import text
from sklearn import decomposition
from collections import defaultdict
from nltk import word_tokenize
from nltk import WordNetLemmatizer
import operator
from collections import Counter
from math import sqrt
from textblob import TextBlob

#Import File
def import_file(filepath):
    rows=[]
    rows=open(filepath, 'r').read().splitlines()
    return rows

#Lemmatize
def lemmatize(token):
    lmtzr = WordNetLemmatizer()
    return lmtzr.lemmatize(token).encode('utf-8') 

#Stop Word List
def load_stopwords():
    stopWords = import_file('C://Users//Nayan Dharamshi//Documents//Python Scripts/stop.txt')
    custom_stop_words=['customer','satisfied','dissatisfied','dissatisfy','cust','good','service','vehicle']    
    for word in custom_stop_words:
        stopWords.append(word)
    return stopWords

#Get back visible element from a html
def visible(element):
    if element.parent.name in ['style', 'script', '[document]', 'head', 'title']:
        return False
    elif re.match('<!--.*-->', str(element)):
        return False
    return True

def f(word):
    return word.encode('utf-8').strip()


#Clean text of symbols and junk spaces
def clean_text(text,stopWords):
    out_text=list()
    for sentence in text:
        clean_text = re.sub('[^0-9a-zA-Z\']',' ',sentence)
        clean_text=re.sub(' +',' ',clean_text)
        new_text=''    
        for word in clean_text.split():
            if word not in stopWords and len(word)>2:
                word=lemmatize(word.encode('utf-8').lower())
                new_text=new_text+' '+word
        out_text.append(new_text)
    return out_text


def clean_word(text,stopWords):
    if text not in stopWords:
        clean_text = re.sub('[^0-9a-zA-Z\']',' ',text)
        clean_text=re.sub(' +',' ',clean_text)
        return clean_text.lower().encode('utf-8')
        

#Build Context Dictionary from all the given Hyperlinks
def create_context_dictionary(links):
    stopWords=load_stopwords()
    context_words=[]
    for link in links:
        html = urllib.urlopen(link).read()
        soup = BeautifulSoup(html)
        texts = soup.findAll(text=True)
        vt = list(filter(visible, texts))
        vt = map(f, vt)
        vt_out=list()
        for vt_element in vt:
            #print vt_element
            if len(vt_element)>0 and vt_element not in stopWords:
                #print vt_element
                vt_bn=re.sub(' +',' ',vt_element)
                vt_bn=re.sub(r'[+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?','',vt_element)
                vt_bn=clean_word(vt_bn,stopWords)
                if vt_bn:
                    vt_out.append(vt_bn.lower())
        wordcount={}
        for word in vt_out:
            for each_word in word.split():
                if len(each_word)>2:
                    if each_word not in wordcount:
                        wordcount[each_word] = 1
                    else:
                        wordcount[each_word] += 1
        sorted_wordcount = sorted(wordcount.items(), key=operator.itemgetter(1),reverse=True)
        
        for k,v in sorted_wordcount:
            if ((len(k)>4 ) or (len(k)>2 and v>2))and k not in stopWords and k not in context_words:
                #print k, v
                context_words.append(k)
    custom_context=['dealer']
    for word in custom_context:
        context_words.append(word)
    return context_words

#Combine context and generic dictionaries to create a master dictionary
def create_master_dictionary(context,generic):
    master_dict=list()
    for word in context:
        master_dict.append(word)
    for word in generic:
        master_dict.append(word)
    return master_dict

#Identify Misspelt word list
def identify_misspelt(n_result,master_dict):
    uniqueWords = [] #get the unique words
    mylist=[]
    for l in range(0,len(n_result)):
        mylist = mylist + n_result[l].split()
    #word_list = [word for line in sentence for word in line.split()] 

    tup_mylist = tuple(mylist)
    x = tup_mylist
    for i in x:
        if not i in uniqueWords:
            uniqueWords.append(i)
           
    refined_list=filter(lambda k:k in uniqueWords, master_dict)
    wrong_word = [word for word in uniqueWords if word not in refined_list]
    used_word=[]
    final_list=[]
    for word in refined_list: #for every word in refined list
        if word not in used_word:
            temp=[]
            temp.append(word.encode('utf-8'))            
            temp1 = difflib.get_close_matches(word, wrong_word,60, 0.8) #Gte the 
            for word1 in temp1:
                temp.append(word1)
#            temp.append(word.encode('utf-8'))
            final_list.append(temp)
            for k in temp:
                used_word.append(k)

    fin_list=final_list
    list_freq = _local_freq_items_p(fin_list, list(), 2)
    list_freq_as_list=list()
    for each in list_freq:
        list_freq_as_list.append(list(each))
    
    for each_dup in list_freq_as_list:
        cmp_cos=word2vec(each_dup[0])
        index_list=list()
        score_list=list()
        for each_list in range(0,len(fin_list)):
            for each_word in fin_list[each_list]:
                if each_word==each_dup[0]:
                    index_list.append(each_list)
                    val_score=word2vec(fin_list[each_list][0])
                    score_list.append(cosdis(cmp_cos,val_score))
        #Identify which index has max score and remove the same
        cnt=0    
        for i in range(0,len(score_list)):
            if score_list[i]==max(score_list) and cnt==0:
                index_list.remove(index_list[i])
                cnt=1
                
        for i in index_list:
            fin_list[i].remove(each_dup[0])
    return fin_list

#Replace misspelt words in the corpus using the correction_list
def replace_misspelt(corpus,fin_list,master_dict,stopWords):
    pre=['in','non', 'im', 'un']    
    j=0
    #corpus=['yesturday workng delivry']
    new_corpus=[]  
    for sentence in corpus: #For every sentence in the corpus
        new_sent=' '
        for word in sentence.split():
            if word not in stopWords:
                if word not in master_dict:        
                    for list_run in fin_list: #For every list in the final list
                        for list_val in list_run: #For every word in the final list
                            if word==list_val:#If word is same as list value                
                                if word == pre[1]+list_run[0]:   #if word is with non im in
                                    word=word
                                word=list_run[0]
                                if(word==list_run[0]):
                                    break
                        if(word==list_run[0]):
                            break       #print word
                new_sent=new_sent +' '+ word
        #nse=list()    
        #nse.append(sentence)
        #nse.append(new_sent)    
        new_corpus.append(new_sent)
        j=j+1
        print(j)

    return new_corpus
    
def word2vec(word):
    cw = Counter(word)
    sw = set(cw)
    lw = sqrt(sum(c*c for c in cw.values()))
    return cw, sw, lw

def cosdis(v1, v2):
    common = v1[1].intersection(v2[1])
    return sum(v1[0][ch]*v2[0][ch] for ch in common)/v1[2]/v2[2]
    
    
def _local_freq_items_p(sdb, prefix, min_support):
    items = defaultdict(int)
    freq_items = []
    for entry in sdb:
        visited = set()
        for element in entry:
            if element not in visited:
                items[element] += 1
                visited.add(element)
    # Sorted is optional. Just useful for debugging for now.
    for item in items:
        support = items[item]
        if support >= min_support:
            freq_items.append((item, support))
    return freq_items


def feature_generation_two(n_set,stopWords,features):
    class LemmaTokenizer(object):
        def __init__(self):
            self.wnl = WordNetLemmatizer()
        def __call__(self, doc):
            return [self.wnl.lemmatize(t) for t in word_tokenize(doc)]
    
    vectorizer_twogram = text.CountVectorizer(max_df=0.95, max_features=features,stop_words=stopWords,ngram_range=(2, 2))
    counts_twogram = vectorizer_twogram.fit_transform(n_set)
    feature_list=vectorizer_twogram.get_feature_names()    

    #two word same please remove
    feature_list
    final_features=[]
    for p in range(0 ,len(feature_list)):
        if len(feature_list[p].split()[0])>2 and len(feature_list[p].split()[1])>2:        
            if feature_list[p].split()[0]!=feature_list[p].split()[1]:
                final_features.append(feature_list[p])
    
    #swapped word removal
    features_voc=[]
    for i in final_features:
        #print i
        for j in final_features:
            if i.split()[0]==j.split()[1] and i.split()[1]==j.split()[0]:
                final_features.remove(i)
   
    return final_features


def feature_generation_one(n_set,stopWords,features):
    class LemmaTokenizer(object):
        def __init__(self):
            self.wnl = WordNetLemmatizer()
        def __call__(self, doc):
            return [self.wnl.lemmatize(t) for t in word_tokenize(doc)]
    
    vectorizer_twogram = text.CountVectorizer(max_df=0.95, max_features=features,stop_words=stopWords,ngram_range=(1, 1))
    counts_twogram = vectorizer_twogram.fit_transform(n_set)
    feature_list=vectorizer_twogram.get_feature_names()    

    
    return feature_list


def topic_extraction(n_set,n_topics=10):
    #n_topics=10
    n_top_words=15
    vectorizer = text.CountVectorizer(max_df=0.95, max_features=50,stop_words=stopWords,ngram_range=(1, 2))
    counts = vectorizer.fit_transform(n_set)
    print vectorizer.get_feature_names()
    tfidf = text.TfidfTransformer().fit_transform(counts)
    #print tfidf
    nmf = decomposition.NMF(n_components=n_topics).fit(tfidf)
    testing= decomposition.NMF(n_components=n_topics).fit_transform(tfidf)
    #print testing.transpose()
    
    #print nmf
    #print nmf.reconstruction_err_        
    
    feature_names = vectorizer.get_feature_names()
    nmf_comps=nmf.components_.tolist()
    for topic_idx, topic in enumerate(nmf.components_):
        print("Topic #%d:" % topic_idx)
        print(" :".join([feature_names[i] for i in topic.argsort()[:-n_top_words - 1:-1]]))
        
def pos_obj(sentence): 
    blob=TextBlob(sentence)
    return(blob.tags)
    
def pos_extraction(sentences):    
    final_pos_list=list()
    nouns_all=list()
    for sent in sentences:
                
        nouns = list()
        adj=list()
        verb=list()
    
        obj=pos_obj(sent)
        for word, tag in obj:
            if (tag =='NN' and word not in nouns):
                nouns.append(word)
            if (tag == 'JJ' and word not in adj):
                adj.append(word)        
            if (tag == 'VB' and word not in verb):
            	verb.append(word)
        
        for noun in nouns:
            if noun not in nouns_all:
                nouns_all.append(noun)
            if len(adj)==0:
                adj.append('blank')
            for adjectives in adj:
                if len(verb)==0:
                    verb.append('blank')
                for verbs in verb:
                    final_pos_list.append([noun,adjectives,verbs])
    return nouns_all    
    
def identify_misspelt_2(n_result,master_dict):
    uniqueWords = [] #get the unique words
    mylist=[]
    for l in range(0,len(n_result)):
        mylist = mylist + n_result[l].split()
    #word_list = [word for line in sentence for word in line.split()] 

    tup_mylist = tuple(mylist)
    x = tup_mylist
    for i in x:
        if not i in uniqueWords:
            uniqueWords.append(i)
           
    refined_list=filter(lambda k:k in uniqueWords, master_dict)
    wrong_word = [word for word in uniqueWords if word not in refined_list]
    used_word=[]
    final_list=[]
    for word in refined_list: #for every word in refined list
        if word not in used_word:
            temp=[]
            temp.append(word.encode('utf-8'))            
            temp1 = difflib.get_close_matches(word, wrong_word,60, 0.8) #Gte the 
            for word1 in temp1:
                temp.append(word1)
#            temp.append(word.encode('utf-8'))
            final_list.append(temp)
            for k in temp:
                used_word.append(k)

    fin_list=final_list
    list_freq = _local_freq_items_p(fin_list, list(), 2)
    list_freq_as_list=list()
    for each in list_freq:
        list_freq_as_list.append(list(each))
    
    for each_dup in list_freq_as_list:
        cmp_cos=word2vec(each_dup[0])
        index_list=list()
        score_list=list()
        for each_list in range(0,len(fin_list)):
            for each_word in fin_list[each_list]:
                if each_word==each_dup[0]:
                    index_list.append(each_list)
                    val_score=word2vec(fin_list[each_list][0])
                    score_list.append(cosdis(cmp_cos,val_score)+0.05*len(fin_list[each_list]))
        #Identify which index has max score and remove the same
        cnt=0    
        for i in range(0,len(score_list)):
            if score_list[i]==max(score_list) and cnt==0:
                index_list.remove(index_list[i])
                cnt=1
                
        for i in index_list:
            fin_list[i].remove(each_dup[0])
    return fin_list
    
#Main Code
stopWords=load_stopwords()
context_words=create_context_dictionary(['http://www.halfordsautocentres.com/advice/servicing-advice/car-servicing-whats-included','http://content.toyotabharat.com/v9online/specs/etios-xclusive-spec-new.htm','http://www.toyota.com/car-tips/basic-car-maintenance-tips-services-checklist.html'])
generic_dictionary=import_file('C://Users//Nayan Dharamshi//Documents//Python Scripts//dict.txt')
master_dict=create_master_dictionary(context_words,generic_dictionary)
data=import_file('C://Users//Nayan Dharamshi//Desktop/inp.txt')
n_result=clean_text(data,stopWords)
fin_list=identify_misspelt_2(n_result,master_dict)
new_data=replace_misspelt(n_result[1:500],fin_list,master_dict,stopWords)
two_gram_features=feature_generation_two(new_data[1:500],stopWords,200)
one_gram_features=feature_generation_one(new_data[1:500],stopWords,300)
topic_extraction(new_data[1:500],n_topics=5)
all_nouns=pos_extraction(new_data[1:500])

#Noun, COntext and topic intersection
int1=refined_list=filter(lambda k:k in context_words, all_nouns)
int2=refined_list=filter(lambda k:k in one_gram_features, int1)

new_two=[]
not_list=['paid','request','requested','required','date','reason']
for element in two_gram_features:
    if (filter(lambda k:k in element.split(), int2) and not filter(lambda k:k in element.split(), not_list)):
        new_two.append(element)
print new_two  
  
cnt=0
for i in range(0,len(new_data)):
    for k in range(0,len(new_two)):
        if new_two[k]in new_data[i]:
            cnt=cnt+1
            print new_two[k], i
            break
    
print cnt


#Db Connection

import pyodbc
cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=NAYANOLPT\SQL12;DATABASE=NLP;')
cursor = cnxn.cursor()
if cursor:
	print "connected"

cursor.execute("delete from dbo.clean_data;commit transaction;")    
for k in range(0,len(new_data)):
    cursor.execute("insert into clean_data(note) values (?)",(new_data[k]))    
cnxn.commit()
cnxn.close()


import pyodbc
cnxn = pyodbc.connect('DRIVER={SQL Server};SERVER=NAYANOLPT\SQL12;DATABASE=NLP;')
cursor = cnxn.cursor()
if cursor:
	print "connected"

cursor.execute("delete from dbo.NOTE_PHRASE;commit transaction;")    
for k in range(0,len(new_two)):
    cursor.execute("insert into NOTE_PHRASE(ATTRPhrase) values (?)",(new_two[k]))    
cnxn.commit()




