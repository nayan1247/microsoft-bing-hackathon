# -*- coding: utf-8 -*-
"""
Created on Sat Jan 16 14:44:03 2016

@author: flusr.in0073
"""

#Model Code
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn import cross_validation
from sklearn import svm
from sklearn import metrics
import csv
import pandas as pd
#import xgboost as xgb

#Extract Specified Columns from the input list
def extract_column(input_list,col_num):
    output_list=[]
    for elem in (input_list):
        output_list.append(elem[col_num])
    return(output_list)
    

#Main Code
text_cols=extract_column(train_data_features,5)
y_cols=extract_column(train_data_features,1)

#Initiate Objects
count_vect = CountVectorizer()
tfidf_transformer = TfidfTransformer()

count_transform=count_vect.fit_transform(text_cols)
tfidf_transform=tfidf_transformer.fit_transform(count_transform)
tfidf_transform.shape
#D=pd.SparseDataFrame([ pd.SparseSeries(tfidf_transform[i].toarray().ravel()) for i in np.arange(tfidf_transform.shape[0]) ])


#Train Test Split
X_train, X_test, y_train, y_test = cross_validation.train_test_split(tfidf_transform, y_cols, test_size=0.3, random_state=0)

from sklearn.naive_bayes import MultinomialNB
from sklearn.linear_model import SGDClassifier

clf = MultinomialNB().fit(X_train,y_train)
clf1=SGDClassifier(loss='hinge', penalty='l2',alpha=1e-3, n_iter=5, random_state=42).fit(X_train,y_train)

predicted = clf.predict(X_test)
print(metrics.classification_report(y_test, predicted))
print(metrics.confusion_matrix(y_test, predicted))

#Cross Validate
clf = svm.SVC(kernel='linear', C=1)
scores = cross_validation.cross_val_score(clf, tfidf_transform, y_cols, cv=5)
scores


D=pd.SparseDataFrame([ pd.SparseSeries(tfidf_transform[i].toarray().ravel()) for i in np.arange(tfidf_transform.shape[0]) ])
D.to_csv('data.csv', sep='\t')


with open('new.csv', 'wb') as csvfile:
   spamwriter = csv.writer(csvfile)
   for row in range(0,len(D)):
       #print row
       spamwriter.writerow(D.loc[row])
       
my_series = pd.Series(y_train)
counts = my_series.value_counts()
counts
